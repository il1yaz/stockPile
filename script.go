package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	bot "stockPile/bot"
	"stockPile/database"
	"stockPile/finnhub"
	"stockPile/moex"
	"strconv"
	"strings"
)

func main() {
	ActualizeDb()
	HttpsServerOn()
}

func GetHelp(ChatId int) {
	text := `Введите подряд названия акции, количество и стоймость в формате

  АКЦИЯ КОЛИЧЕСТВО ЦЕНА (GAZP 5 150)

  По завершению введите команду \finish`
	bot.SendMessage(ChatId, text)
}

func AddStockInWallet(UserId int, ChatId int, RawData string) {
	var Data []string
	Data = strings.Split(RawData, " ")
	log.Println(Data[0] + " " + Data[1] + " " + Data[2])
	if len(Data) == 3 && database.GetStockFullName(Data[0]) != "" {
		stockValue, _ := strconv.Atoi(Data[1])
		stockPrice, _ := strconv.ParseFloat(Data[2], 64)
		database.InsertUserStockData(UserId, Data[0], stockValue, stockPrice)
	} else {
		bot.SendMessage(ChatId, "Введены неверные данные, повторите попытку")
	}
}

// var commands map[String]interface{
// 	"create":
// }

func ActualizeDb() {
	database.ConnectToDb()
	moexStocks, err := moex.GetStockNamesFromMoex()
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Check Moex")
	for _, moexStock := range moexStocks {
		if database.GetStockFullName(moexStock.Code) == "" {
			log.Println("Insert " + moexStock.Code)
			database.InsertStockData(moexStock.Code, moexStock.Name, "MOEX")
		}
	}
	finnhubStocks, err := finnhub.GetStockSymbolsFromFinnhub()
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Check Finnhub")
	for _, finnhubStock := range finnhubStocks {
		if finnhubStock.Type == "EQS" && database.GetStockFullName(finnhubStock.DisplaySymbol) == "" {
			log.Println("Insert" + finnhubStock.DisplaySymbol)
			database.InsertStockData(finnhubStock.DisplaySymbol, finnhubStock.Description, "FINNHUB")
		}
	}
}

func HelloServer(w http.ResponseWriter, req *http.Request) {
	body := &bot.UpdateResultT{}
	if err := json.NewDecoder(req.Body).Decode(body); err != nil {
		fmt.Println("could not decode request body", err)
		return
	}
	fmt.Println(body)
	if strings.Count(body.Message.Text, " ") == 2 {
		AddStockInWallet(body.Message.From.Id, body.Message.Chat.Id, body.Message.Text)
	} else {
		bot.SendMessage(body.Message.Chat.Id, "Введены неверные данные, повторите попытку")
	}

}

func HttpsServerOn() {
	http.HandleFunc("/", HelloServer)
	err := http.ListenAndServeTLS(":8443", "./certs/server1.crt", "./certs/server1.key", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
