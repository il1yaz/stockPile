package finnhub

import (
	"encoding/json"
	"log"
	request "stockPile/requests"
)

type FinnhubStockPriceAnswer struct {
	CurrentPrice float32 `json:"c"`
}

type FinnhubSymbolsBody struct {
	Currency      string `json:"currency"`
	Description   string `json:"description"`
	DisplaySymbol string `json:"displaySymbol"`
	Symbol        string `json:"symbol"`
	Type          string `json:"type"`
}

const FinnhubUrl = "https://finnhub.io/api/v1"
const StockPriceUrlParams = "/quote?symbol="
const StockNames = "/stock/symbol"
const SymbolsParamUrl = "?exchange=US"
const token = "bsa5qbvrh5rfukjh03dg"

func GetStockPriceFromFinnhub(FinnhubStockName string) (float32, error) {
	RequestUrl := FinnhubUrl + StockPriceUrlParams + FinnhubStockName + "&token=" + token
	respJson, err := parseStockPriceResponse(request.MakeRequest(RequestUrl))
	if err != nil {
		log.Fatalln(err)
		return -1.0, err
	}
	return respJson.CurrentPrice, nil
}

func GetStockSymbolsFromFinnhub() ([]FinnhubSymbolsBody, error) {
	RequestUrl := FinnhubUrl + StockNames + SymbolsParamUrl + "&token=" + token
	respJson, err := parseStockSymbolsFromFinnhub(request.MakeRequest(RequestUrl))
	if err != nil {
		log.Fatalln(err)
		return respJson, err
	}
	return respJson, nil
}

func parseStockPriceResponse(body []byte) (FinnhubStockPriceAnswer, error) {
	finnhubStockPriceAnswer := FinnhubStockPriceAnswer{}
	err := json.Unmarshal(body, &finnhubStockPriceAnswer)
	if err != nil {
		log.Fatalln(err)
	}
	return finnhubStockPriceAnswer, nil
}

func parseStockSymbolsFromFinnhub(body []byte) ([]FinnhubSymbolsBody, error) {
	finnhubSymbols := []FinnhubSymbolsBody{}
	err := json.Unmarshal(body, &finnhubSymbols)
	if err != nil {
		log.Fatalln(err)
	}
	return finnhubSymbols, nil
}
