package bot

import (
	"encoding/json"
	"log"
	request "stockPile/requests"
	"strconv"
)

const telegramApiUrl = "https://api.telegram.org/"
const botApiToken = "bot483497540:AAGW8GD9_hjLAPg_DgTkL7ErdRAJR7jKwUA"

type UpdateT struct {
	Ok     bool            `json:"ok"`
	Result []UpdateResultT `json:"result"`
}

type UpdateResultT struct {
	UpdateId int                  `json:"update_id"`
	Message  UpdateResultMessageT `json:"message"`
}

type UpdateResultMessageT struct {
	MessageId int               `json:"message_id"`
	From      UpdateResultFromT `json:"from"`
	Chat      UpdateResultChatT `json:"chat"`
	Date      int               `json:"date"`
	Text      string            `json:"text"`
}

type UpdateResultFromT struct {
	Id        int    `json:"id"`
	IsBot     bool   `json:"is_bot"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Username  string `json:"username"`
	Language  string `json:"language_code"`
}

type UpdateResultChatT struct {
	Id        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Username  string `json:"username"`
	Type      string `json:"type"`
}

type SendMessageResponseT struct {
	Ok     bool                 `json:"ok"`
	Result UpdateResultMessageT `json:"message"`
}

func SendMessage(chatId int, text string) SendMessageResponseT {
	url := telegramApiUrl + botApiToken + "/sendMessage" + "?chat_id=" + strconv.Itoa(chatId) + "&text=" + text
	response := request.MakeRequest(url)
	sendMessage := SendMessageResponseT{}
	err := json.Unmarshal(response, &sendMessage)
	if err != nil {
		log.Fatalln(err)
	}
	return sendMessage
}

func SendReplyedMessage(chatId int, messageId int, text string) SendMessageResponseT {
	url := telegramApiUrl + botApiToken +
		"/sendMessage" + "?chat_id=" + strconv.Itoa(chatId) +
		"&reply_to_message_id=" + strconv.Itoa(messageId) +
		"&text=" + text
	response := request.MakeRequest(url)
	sendReplyedMessage := SendMessageResponseT{}
	err := json.Unmarshal(response, &sendReplyedMessage)
	if err != nil {
		log.Fatalln(err)
	}
	return sendReplyedMessage
}
