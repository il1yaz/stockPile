package requests

import (
	"io/ioutil"
	"log"
	"net/http"
)

func MakeRequest(Url string) []byte {
	resp, err := http.Get(Url)
	if err != nil {
		log.Fatalln(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		log.Fatalln(err)
	}
	return body
}
