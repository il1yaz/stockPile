package database

import (
	"database/sql"
	"log"
	"time"

	_ "github.com/lib/pq"
)

var (
	db *sql.DB
)

type Stock struct {
	code   string
	name   string
	broker string
}

func ConnectToDb() *sql.DB {
	var sqlError error
	db, sqlError = sql.Open("postgres", "postgres://postgres:postgres@localhost:5432/postgres?sslmode=disable")
	if sqlError != nil {
		log.Fatalln(sqlError)
	} else {
		log.Println("Database connection established")
	}
	return db
}

func InsertStockData(code string, name string, broker string) {
	sqlInsertRequest := "INSERT INTO stocks(code,name,broker) VALUES($1, $2, $3)"
	result, err := db.Exec(sqlInsertRequest, code, name, broker)
	if err != nil {
		log.Println(err)
	}
	log.Println(result)
}

func GetStockFullName(code string) string {
	sqlRequest := "Select * from stocks where code=$1"
	result := db.QueryRow(sqlRequest, code)
	resultStock := Stock{}
	error := result.Scan(&resultStock.code, &resultStock.name, &resultStock.broker)
	if error != nil {
		return ""
	}
	return resultStock.name
}

func InsertUserStockData(userId int, stockName string, value int, buyPrice float64) {
	sqlRequest := "INSERT INTO users_wallet(\"userId\", \"stockName\", \"stockValue\", \"buyPrice\") VALUES($1, $2, $3, $4)"
	_, err := db.Exec(sqlRequest, userId, stockName, value, buyPrice)
	if err != nil {
		log.Fatalln(err)
	}
}

func InsertChatUpdates(updateId int64) {
	sqlInsertRequest := "INSERT INTO updates(updateid, answertime) VALUES($1, $2)"
	_, err := db.Exec(sqlInsertRequest, updateId, time.Now())
	if err != nil {
		log.Println(err)
	}
}

// func CheckChatUpdates(updateId int64) {
// 	sqlRequest := "SELECT * from updates where updateid=$1"
// }
