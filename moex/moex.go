//// TODO: парсить ответ с названиями Акций https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities?iss.only=securities&securities.columns=SECID,SECNAME
package moex

import (
	"encoding/json"
	"fmt"
	"log"
	request "stockPile/requests"
)

type MoexPriceAnswer struct {
	Securities SecuritiesT `json:"marketdata"`
}

type MoexStockNameAnswer struct {
	Securities Stock `json:"securities"`
}

type Stock struct {
	Data []StockInfo `json:"data"`
}

type StockInfo struct {
	Code string
	Name string
}

type SecuritiesT struct {
	Data [][]float32 `json:"data"`
}

const MoexUrl = "https://iss.moex.com/iss/engines/stock/markets/shares/boards/TQBR/securities"
const MoexPriceRequestParameters = ".json?iss.only=marketdata&marketdata.columns=LAST"
const MoexStocksNameRequest = ".json?iss.only=securities&securities.columns=SECID,SECNAME"

func (si *StockInfo) UnmarshalJSON(d []byte) error {
	val := []string{}
	err := json.Unmarshal(d, &val)
	if err != nil {
		return err
	}
	if len(val) != 2 {
		return fmt.Errorf("StockInfo unmarshal error len != 2")
	}
	si.Code = val[0]
	si.Name = val[1]
	return nil
}

func GetStockPriceFromMoex(MoexStockName string) (float32, error) {
	RequestUrl := MoexUrl + "/" + MoexStockName + MoexPriceRequestParameters

	resp := request.MakeRequest(RequestUrl)
	moexAnswer := MoexPriceAnswer{}
	err := json.Unmarshal(resp, &moexAnswer)
	if err != nil || len(moexAnswer.Securities.Data) == 0 {
		log.Println(err)
		return -1.0, err
	}
	return moexAnswer.Securities.Data[0][0], nil
}

func GetStockNamesFromMoex() ([]StockInfo, error) {
	RequestUrl := MoexUrl + MoexStocksNameRequest
	resp := request.MakeRequest(RequestUrl)
	moexAnswer := MoexStockNameAnswer{}
	err := json.Unmarshal(resp, &moexAnswer)
	if err != nil || len(moexAnswer.Securities.Data) == 0 {
		log.Println(err)
	}
	return moexAnswer.Securities.Data, err
}

func parseResponse(body []byte) (MoexPriceAnswer, error) {
	moexAnswer := MoexPriceAnswer{}
	err := json.Unmarshal(body, &moexAnswer)
	if err != nil {
		log.Fatalln(err)
	}
	return moexAnswer, nil

}
